/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";

function SideNav() {
  return (
    <div className="flex flex-col gap-y-8 p-3">
      <div className="w-[46px] h-[46px] bg-gray-100 mx-auto text-black font-extrabold p-2 rounded-md">
        MA
      </div>
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black font-extrabold rounded-md">
        <img src="/images/dashboard.svg" alt="dashboard" className=" mx-auto" />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Dashboard
        </span>
      </div>
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black   font-extrabold   rounded-md  ">
        <img
          src="/images/auditing.svg"
          alt="auditing"
          className=" mx-auto mt-2"
        />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Auditing
        </span>
      </div>{" "}
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black   font-extrabold   rounded-md  ">
        <img
          src="/images/message.svg"
          alt="message"
          className=" mx-auto mt-2"
        />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Messages
        </span>
      </div>{" "}
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black   font-extrabold   rounded-md  ">
        <img
          src="/images/clients.svg"
          alt="clients"
          className=" mx-auto mt-2"
        />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Your Clients
        </span>
      </div>{" "}
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black mt-32  font-extrabold   rounded-md  ">
        <img
          src="/images/setting_icon.svg"
          alt="clients"
          className=" mx-auto mt-2"
        />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Settings
        </span>
      </div>{" "}
      <div className="w-[46px] h-[46px]  flex flex-col  items-center hover:bg-[#7481D7] bg-none text-black my-10  font-extrabold   rounded-md  ">
        <img
          src="/images/user_icon.svg"
          alt="clients"
          className="mx-auto mt-2"
        />
        <span className="text-center text-sm font-normal mx-auto text-white">
          Users
        </span>
      </div>
    </div>
  );
}

function Header(props) {
  const { handleClick, menu } = props;

  return (
    <div className=" px-5 h-[60px]  flex justify-between bg-white shadow-sm">
      <button onClick={handleClick} className="md:hidden">
        {!menu ? (
          <img src="/icons/menuicon.svg" alt="menu" />
        ) : (
          <img src="/icons/cancel.svg" alt="close" />
        )}
      </button>
      <div className=" flex gap-x-3 items-center">
        <img
          src=" /images/arrow_back.svg"
          alt="back"
          className=" border p-2 bg-[#EBEDF9] rounded-full"
        />
        <p className=" text-[#212121] font-bold text-md">Audit Finding</p>
      </div>
      <div className="  flex gap-x-3 items-center">
        <img src=" /images/notification_icon.svg" alt="notification" />
        <p className="border p-2 bg-[#EBEDF9] rounded-full text-[14px] font-extrabold ">
          GP
        </p>{" "}
      </div>
    </div>
  );
}

function AboutUs() {
  const [showModal, setShowModal] = useState(false);

  return (
    <div className=" px-5 my-10 flex md:flex-row flex-col gap-y-3 justify-between">
      <div className=" flex gap-x-3 items-center">
        <div>
          <span className=" text-[16px] font-bold ">
            Star Technologies, Mumbai
          </span>
          <br />
          <span className=" text-[14px] font-normal">
            01-Apr-2022 To 31-Mar-2023
          </span>
        </div>
        <div className=" flex gap-x-3 items-center">
          <img src=" /images/Line 61.svg" alt="line" />
          <button className=" text-[16px] font-bold">Run Audit</button>

          <img
            src=" /images/play.svg"
            alt="run"
            onClick={() => setShowModal(!showModal)}
          />
        </div>

        <AuditProgress showModal={showModal} setShowModal={setShowModal} />
      </div>

      <div className=" border-2 text-[#29368E] whitespace-nowrap  flex items-center justify-center text-[16px] font-semibold rounded-lg md:w-[20%] w-[80%]  h-16 border-[#C4C9EE] hover:bg-[#29368E] hover:text-white">
        Audit Manually
      </div>
    </div>
  );
}

function PurchaseDetails() {
  return (
    <div className=" flex md:flex-row flex-col gap-5 px-5 justify-center gap-x-5">
      <div className=" flex flex-col gap-y-3  w-full">
        <div className=" text-[16px]  border border-gray-200 shadow-md rounded-lg p-5">
          <h3 className=" text-xl font-bold">Assets / Liabilities</h3>
          <div className=" grid md:grid-cols-2 gap-3">
            <div className=" bg-[#EBEDF9] h-[107px] leading-7 p-5 rounded-lg">
              <h4 className=" font-semibold">Fixed Assets</h4>
              <span className=" font-semibold text-[#29368E]">₹ 39,52,962</span>
            </div>
            <div className=" bg-[#EBEDF9] h-[107px] leading-7 p-5 rounded-lg">
              <h4 className=" font-semibold">Outstanding Loans</h4>
              <span className=" font-semibold text-[#29368E]">₹ 10,29,740</span>
            </div>
            <div className=" bg-[#EBEDF9] h-[107px] leading-7 p-5 rounded-lg">
              <h4 className=" font-semibold">Cash Balance</h4>
              <span className=" font-semibold text-[#29368E]">₹ 35,91,586</span>
            </div>
            <div className=" bg-[#EBEDF9] h-[107px] leading-7 p-5 rounded-lg">
              <h4 className=" font-semibold">Bank Balance</h4>
              <span className=" font-semibold text-[#29368E]">₹ 4,87,705</span>
            </div>
          </div>
        </div>
        <div className=" text-[16px]  border border-gray-200 shadow-md rounded-lg p-5">
          <div className=" flex justify-between items-center">
            <h3 className=" text-xl font-bold">Top 5 expenses</h3>
            <button className="border border-gray-300 py-1 px-2 rounded-3xl">
              View All
            </button>
          </div>

          <div className=" flex flex-row  justify-between items-center">
            <img
              src="/images/chart_1.svg"
              alt="chart1 "
              className="md:h-46 md|:w-46"
            />
            <ul className=" grid md:grid-cols-2 gap-5">
              <li className=" flex items-baseline gap-x-3">
                <img src="/images/bullet1.svg" alt="bulter1" />
                <div>
                  <div className=" font-semibold">53%</div>
                  <div className="  text-xs">Employee Benefit expenses</div>
                </div>
              </li>
              <li className=" flex items-baseline gap-x-3">
                <img src="/images/bullet4.svg" alt="bullet2" />
                <div>
                  <div className=" font-semibold">4%</div>
                  <div className=" text-xs">Repairs & Maintenance</div>
                </div>
              </li>
              <li className=" flex items-baseline gap-x-3">
                <img src="/images/bullet2.svg" alt="bullet" />
                <div>
                  <div className=" font-semibold">25%</div>
                  <div className=" text-xs">Rental Expense</div>
                </div>
              </li>
              <li className=" flex items-baseline gap-x-3">
                <img src="/images/bullet5.svg" alt="bullet" />
                <div>
                  <div className=" font-semibold">5%</div>
                  <div className=" text-xs">Office Maintenance</div>
                </div>
              </li>
              <li className=" flex items-baseline gap-x-3">
                <img src="/images/bullet3.svg" alt="bullet" />
                <div>
                  <div className=" font-semibold">13%</div>
                  <div className=" text-xs">Administrative</div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className=" text-[16px]  border border-gray-200 shadow-md rounded-lg p-5 w-full ">
        <h3 className=" text-xl font-bold">Revenue</h3>
        <div className=" mt-5 grid grid-cols-2 gap-5">
          <div className=" bg-[#EBEDF9] h-[83px] leading-7 p-2 rounded-lg">
            <h4 className=" font-semibold">Total Turn Over</h4>
            <span className=" font-semibold text-[#29368E]">₹ 9,33,78,890</span>
          </div>
          <div className=" bg-[#EBEDF9] h-[83px] leading-7 p-2 rounded-lg">
            <h4 className=" font-semibold">Net Profit</h4>
            <span className=" font-semibold text-[#29368E]">₹ 19,22,664</span>
          </div>
        </div>
        <div className=" flex mt-5 gap-x-10 justify-center items-center">
          <img src="/images/chart_2.svg" alt="chart1 " className="h-46 w-46 " />
          <ul className=" grid md:grid-cols-1 gap-5">
            <li className=" flex items-baseline gap-x-3">
              <img src="/images/bullet3.svg" alt="bulter1" />
              <div>
                <div className=" font-semibold">51%</div>
                <div className="  text-sm">Total Turnover</div>
              </div>
            </li>
            <li className=" flex items-baseline gap-x-3">
              <img src="/images/bullet4.svg" alt="bullet2" />
              <div>
                <div className=" font-semibold">5%</div>
                <div className=" text-sm">Total Expenses</div>
              </div>
            </li>
            <li className=" flex items-baseline gap-x-3">
              <img src="/images/bullet6.svg" alt="bullet" />
              <div>
                <div className=" font-semibold">45%</div>
                <div className=" text-sm">Total Purchase</div>
              </div>
            </li>
          </ul>
        </div>
        <div className=" flex flex-row font-semibold gap-5 mt-5 justify-center items-center ">
          <ul className=" leading-10 ">
            <li>Total Purchase:</li>
            <li>Total Direct Expenses:</li>
            <li>Total Indirect Expenses:</li>
            <li>Chaange in Stock:</li>
          </ul>
          <ul className=" leading-10 text-sm">
            <li>Rs. 8,31,89,497.71</li>
            <li>Rs. 13,82,664.33</li>
            <li>Rs. 82,66,727.96</li>
            <li>Rs. 5,40,000</li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function TopCustomers() {
  return (
    <div className=" flex md:flex-row flex-col gap-5 mt-5 px-5 justify-center gap-x-5">
      <div className=" text-[16px]  border border-gray-200 shadow-md rounded-lg p-5 w-full">
        <div className=" flex justify-between items-center">
          <h3 className=" text-xl font-bold">Top Debitors</h3>
          <button className="border border-gray-300 py-1 px-2 rounded-3xl">
            View All
          </button>
        </div>
        <div className=" flex flex-row font-semibold gap-5 mt-5 justify-between items-center ">
          <ul className=" leading-10 ">
            <li>Card Sales Customers:</li>
            <li>KP Retail:</li>
            <li>Otion Hospital:</li>
            <li>Season Travels:</li>
            <li>ODPC Power Limited</li>
          </ul>
          <ul className=" leading-10 text-sm">
            <li>Rs. 11,22,923.48</li>
            <li>Rs. 4,17,086.0</li>
            <li>Rs. 1,20,345.88</li>
            <li>Rs. 51,000.0</li>
            <li>Rs. 18,498.0</li>
          </ul>
        </div>
      </div>
      <div className=" text-[16px]  border border-gray-200 shadow-md rounded-lg p-5 w-full">
        <div className=" flex justify-between items-center">
          <h3 className=" text-xl font-bold">Top Creditors</h3>
          <button className="border border-gray-300 py-1 px-2 rounded-3xl">
            View All
          </button>
        </div>
        <div className=" flex flex-row font-semibold gap-5 mt-5 justify-between items-center ">
          <ul className=" leading-10 ">
            <li>Sk Mills</li>
            <li>SSD Chocolates</li>
            <li>RK Hotels</li>
            <li>Star Bakers</li>
            <li>Season Travels</li>
          </ul>
          <ul className=" leading-10 text-sm">
            <li>Rs. 11,17,1443.21</li>
            <li>Rs. 6,73,521.0</li>
            <li>Rs. 5,32,128.0</li>
            <li>Rs. 4,77,497.0</li>
            <li>Rs. 3,81,871.0</li>
          </ul>
        </div>
      </div>
    </div>
  );
}

function HomeUi() {
  const [menu, setMenu] = useState("");

  const handleClick = () => {
    setMenu(!menu);
  };

  return (
    <div
      className="bg-[#FAFAFA] w-[100%]
    "
    >
      <div className=" flex">
        {menu ? (
          <div className="w-full md:hidden bg-[#29368E]">
            <SideNav />
          </div>
        ) : null}

        <div className="w-[7%] bg-[#29368E] md:block hidden">
          <SideNav />
        </div>
        <div className="w-full">
          <Header handleClick={handleClick} menu={menu} />
          <AboutUs />
          <PurchaseDetails />
          <TopCustomers />
        </div>
      </div>
    </div>
  );
}

export default HomeUi;

function AuditProgress(props) {
  const showModal = props.showModal;
  const setShowModal = props.setShowModal;

  if (!showModal) {
    return null;
  }

  return (
    <div
      className="fixed inset-0 z-10 overflow-y-auto"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
    >
      <div className="flex min-h-screen items-center justify-center px-4 pt-4 pb-20 text-center sm:block sm:p-0">
        <div
          className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
          aria-hidden="true"
        ></div>

        <span
          className="hidden sm:inline-block sm:h-screen sm:align-middle"
          aria-hidden="true"
        >
          &#8203;
        </span>

        <div className="relative inline-block max-w-sm transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left align-bottom shadow-xl transition-all sm:my-8 sm:w-full sm:p-6 sm:align-middle">
          <img
            src="/icons/close-icon.svg"
            className="absolute top-2 right-3 cursor-pointer bg-red-600 py-2 px-2 sm:top-10 md:right-10 md:top-8"
            alt="close"
            onClick={() => setShowModal(!showModal)}
          />
          <div className="text-center leading-10">
            <img src="/icons/reload.svg" alt="reload" className="mx-auto" />
            <p className=" text-[18px] font-bold">Auditing is in progress</p>
            <p className=" text-[16px]">
              Do not close or refresh the page. This process may take some time
              to complete
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
